import React from 'react';
import Button from './../graphicElements/button/button';
import RadioButtonsGroup from './../graphicElements/radiobuttons/RadioButtonsGroup';
import CheckButtonsGroup from '../graphicElements/checkbuttons/CheckButtonsGroup';
import TextView from '../graphicElements/TextView/TextView';

export default class GenericBubble extends React.Component {

    functionForTheButton() {
        this.alert('prova notifica');
    }

    render() {
        return (
            <div>
                <TextView text="This is a textview">What about this?</TextView>
                <Button function={this.functionForTheButton} text="bottone" />
                <RadioButtonsGroup
                  groupName="payments" buttons={[
                      {
                          value: 'paypal',
                          label: 'PayPal',
                          checked: true,
                      },
                      {
                          value: 'visa',
                          label: 'VISA',
                      },
                      {
                          value: 'mastercard',
                          label: 'MasterCard',
                      },
                      {
                          value: 'contanti',
                          label: 'BIG MONEY CARTACEI!',
                      },
                  ]}
                />
                <CheckButtonsGroup
                  groupName="check" buttons={[
                      {
                          name: 'paypal',
                          value: 'paypal',
                          label: 'PayPal',
                          checked: true,
                      },
                      {
                          name: 'visa',
                          value: 'visa',
                          label: 'VISA',
                          checked: true,
                      },
                      {
                          name: 'mastercard',
                          value: 'mastercard',
                          label: 'MasterCard',
                          checked: true,
                      },
                      {
                          name: 'contanti',
                          value: 'contanti',
                          label: 'BIG MONEY CARTACEI!',
                          checked: false,
                      },
                  ]}
                />
            </div>
        );
    }
}
